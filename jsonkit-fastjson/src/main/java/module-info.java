module jsonkit.fastjson {
    requires jsonkit.core;
    requires data.core;
    requires data.fastjson;
    exports com.ejlchina.json.fastjson;
}