module jsonkit.jackson {
    requires jsonkit.core;
    requires data.core;
    requires data.jackson;
    exports com.ejlchina.json.jackson;
}